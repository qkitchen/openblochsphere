from manimlib.imports import *
from qubit import *

OUTPUT_DIRECTORY = "qubit"

class HadamardRotate(BlochSphereHadamardRotate):

    CONFIG = {
        "rotate_sphere": True,
        "rotate_time": 5,
        "rotate_amount": 1,
    }

class PauliX(BlochSphere):

    CONFIG = {
        "operators": [Pauli_x,],
        "operator_names":["Pauli X",],
    }
    
class PauliY(BlochSphere):

    CONFIG = {
        "operators": [Pauli_y,],
        "operator_names": ["Pauli Y",],
    }

class PauliZ(BlochSphere):

    CONFIG = {
        "operators": [Pauli_z,],
        "operator_names": ["Pauli Z",],
    }

class PauliX_X(BlochSphere):

    CONFIG = {
        "operators": [Pauli_x,Pauli_x,],
        "operator_names":["Pauli X","Pauli X",],
    }

class PauliY_Y(BlochSphere):

    CONFIG = {
        "operators": [Pauli_y,Pauli_y,],
        "operator_names":["Pauli Y","Pauli Y",],
    }

class PauliZ_Z(BlochSphere):

    CONFIG = {
        "operators": [Pauli_z,Pauli_z,],
        "operator_names":["Pauli Z","Pauli Z",],
    }

class Hadamard_P45(BlochSphere):
 
    CONFIG = {
        "operators": [Hadamard,Phase(45*DEGREES),],
        "operator_names":["Hadamard","Phase 45",],
    }

class Hadamard_P90(BlochSphere):
   
    CONFIG = {
        "operators": [Hadamard,Phase(90*DEGREES),],
        "operator_names":["Hadamard","Phase 90",],
    }

class Hadamard_P180(BlochSphere):
  
    CONFIG = {
        "operators": [Hadamard,Phase(180*DEGREES),],
        "operator_names":["Hadamard","Phase 180",],
    }

class Hadamard_P270(BlochSphere):

    CONFIG = {
        "operators": [Hadamard,Phase(270*DEGREES),],
        "operator_names":["Hadamard","Phase 270",],
    }
    
class Hadamard_2(BlochSphere):

    CONFIG = {
        "operators": [Hadamard, Hadamard],
        "operator_names":["Hadamard","Hadamard",],
    }

class PauliX_Y(BlochSphere):
      
    CONFIG = {
        "operators": [Pauli_x,Pauli_y,],
        "operator_names":["Pauli X","Pauli Y",],
    }

class PauliX_Z(BlochSphere):

    CONFIG = {
        "operators": [Pauli_x,Pauli_z,],
        "operator_names":["Pauli X","Pauli Z",],
    }

class PauliY_X(BlochSphere):
   
    CONFIG = {
        "operators": [Pauli_y,Pauli_x,],
        "operator_names":["Pauli Y","Pauli X",],
    }

class PauliY_Z(BlochSphere):
  
    CONFIG = {
        "operators": [Pauli_y,Pauli_z,],
        "operator_names":["Pauli Y","Pauli Z",],
    }

class PauliZ_X(BlochSphere):

    CONFIG = {
        "operators": [Pauli_z,Pauli_x,],
        "operator_names":["Pauli Z","Pauli X",],
    }

class PauliZ_Y(BlochSphere):

    CONFIG = {
        "operators": [Pauli_z,Pauli_y,],
        "operator_names":["Pauli Z","Pauli Y",],
    }

class Hadamard_X(BlochSphere):
 
    CONFIG = {
        "operators": [Hadamard,Pauli_x,],
        "operator_names":["Hadamard","Pauli X",],
    }

class Hadamard_Y(BlochSphere):
  
    CONFIG = {
        "operators": [Hadamard,Pauli_y,],
        "operator_names":["Hadamard","Pauli Y",],
    }

class Hadamard_Z(BlochSphere):
   
    CONFIG = {
        "operators": [Hadamard,Pauli_z,],
        "operator_names":["Hadamard","Pauli Z",],
    }

class Hadamard_X_H(BlochSphere):
  
    CONFIG = {
        "operators": [Hadamard,Pauli_x,Hadamard],
        "operator_names":["Hadamard","Pauli X","Hadamard",],
    }

class Hadamard_Y_H(BlochSphere):
   
    CONFIG = {
        "operators": [Hadamard,Pauli_y,Hadamard],
        "operator_names":["Hadamard","Pauli Y","Hadamard",],
    }

class Hadamard_Z_H(BlochSphere):
   
    CONFIG = {
        "operators": [Hadamard,Pauli_z,Hadamard],
        "operator_names":["Hadamard","Pauli Z","Hadamard",],
    }







